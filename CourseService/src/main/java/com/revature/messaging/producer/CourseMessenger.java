package com.revature.messaging.producer;

import javax.inject.Inject;

import org.jboss.logging.Logger;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import com.revature.messaging.config.RabbitConfig;
import com.revature.models.Course;

@Component
public class CourseMessenger {
	
	private RabbitConfig config;
	
	Logger log = Logger.getLogger(CourseMessenger.class);
	
	private final RabbitTemplate rabbitTemplate;
	
	@Inject
	public CourseMessenger(RabbitTemplate rabbitTemplate, @Lazy RabbitConfig rabbitConfig) {
		this.rabbitTemplate = rabbitTemplate;
		this.config = rabbitConfig;
		rabbitTemplate.setConfirmCallback( (correlationData, ack, cause) -> {
			if(!ack) log.error("Message confirmation failure: " + cause);
			else log.info("Message confirmed.");
		});
		rabbitTemplate.setMandatory(true);
		
	}

	public void sendCreationMessage(Course course) {
		log.warn("Exchange: " + config.getEXCHANGE() + ", routing-key: " + config.getCOURSE_CREATE_ROUTE());
		rabbitTemplate.convertAndSend(config.getEXCHANGE(), config.getCOURSE_CREATE_ROUTE(), course);
	}
}
