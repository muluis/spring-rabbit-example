package com.revature.messaging.listeners;

import org.jboss.logging.Logger;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.revature.models.Signup;
import com.revature.models.Student;
import com.revature.services.StudentService;

@Component
public class SignupListener {
	
	StudentService studentService;
	private final String studentQueue;
	
	Logger log = Logger.getLogger(SignupListener.class);
	
	@RabbitListener(queues = "student-queue")
	public void receiveMessage(final Signup signup) {
		log.error("Received deletion message for signup:" + signup);
		Student student = studentService.getStudent(signup.getStudentId());
		log.error("Student bumped from class! Send an e-mail! " + student);
	}

	public SignupListener(StudentService studentService, 
			@Value("${spring.rabbitmq.queues.student:student-queue") String studentQueue) {
		super();
		this.studentService = studentService;
		this.studentQueue = studentQueue;
	}
}
