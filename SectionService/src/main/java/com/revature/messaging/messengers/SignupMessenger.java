package com.revature.messaging.messengers;

import javax.inject.Inject;

import org.jboss.logging.Logger;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.revature.models.Signup;

@Component
public class SignupMessenger {
	
	Logger log = Logger.getLogger(SignupMessenger.class);
	
	private final RabbitTemplate rabbitTemplate;
	
	@Value("${spring.rabbitmq.exchange:assignforce}")
	private String exchange;
	
	@Value("${spring.rabbitmq.routing.signup.delete:assignforce.signup.delete}")
	private String routingKey;

	@Inject
	public SignupMessenger(RabbitTemplate rabbitTemplate) {
		super();
		this.rabbitTemplate = rabbitTemplate;
	}

	public void sendDeletionMessage(Signup signup) {
		
		log.info("Sending deletion message for: " + signup);
		log.info("Exchange: " + exchange +", RoutingKey: " + routingKey);
		rabbitTemplate.convertAndSend(exchange, routingKey, signup);
	}
	
}
